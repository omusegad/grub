<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHomecontentfoodTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('homecontentfood', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('vCode', 100);
			$table->text('header_first_label', 65535)->comment('home_first_first_title');
			$table->string('header_second_label', 350)->comment('home_second_first_title');
			$table->string('home_banner_left_image', 350)->comment('home_banner_image');
			$table->text('left_banner_txt', 65535);
			$table->string('home_banner_right_image', 350)->comment('home_second_right_banner_image');
			$table->text('right_banner_txt', 65535);
			$table->string('third_sec_title', 500)->comment('home_third_first_title');
			$table->text('third_sec_desc', 65535)->comment('home_first_second_texthome_first_second_content');
			$table->string('third_mid_image_one', 350);
			$table->string('third_mid_title_one', 500)->comment('home_third_second_text');
			$table->text('third_mid_desc_one', 65535)->comment('home_first_four_button_content');
			$table->string('third_mid_image_two', 350)->comment('home_third_banner_image');
			$table->string('third_mid_title_two', 500)->comment('home_fours_first_text');
			$table->text('third_mid_desc_two', 65535)->comment('home_second_second_content');
			$table->string('third_mid_image_three', 350)->comment('home_fours_icon_image');
			$table->string('third_mid_title_three', 500)->comment('home_fours_second_text');
			$table->text('third_mid_desc_three', 65535)->comment('home_third_third_content');
			$table->string('mobile_app_bg_img', 350);
			$table->string('mobile_app_left_img', 350);
			$table->string('mobile_app_right_title', 500)->comment('home_six_first_title');
			$table->text('mobile_app_right_desc', 65535)->comment('home_six_second_content');
			$table->string('taxi_app_bg_img', 350)->comment('home_five_left_banner_image');
			$table->string('taxi_app_left_img', 350)->comment('home_six_banner_image');
			$table->string('taxi_app_right_title', 500);
			$table->text('taxi_app_right_desc', 65535)->comment('home_five_second_content');
			$table->text('driver_sec_first_label', 65535);
			$table->text('driver_sec_second_label', 65535);
			$table->enum('eStatus', array('Active','Inactive'))->default('Active');
			$table->string('third_mid_image_one1', 350);
			$table->string('third_mid_title_one1', 500)->comment('home_five_first_title');
			$table->text('third_mid_desc_one1', 65535);
			$table->string('third_mid_image_two1', 350);
			$table->string('third_mid_title_two1', 500);
			$table->text('third_mid_desc_two1', 65535)->comment('home_first_third_button_content');
			$table->string('third_mid_image_three1', 350);
			$table->string('third_mid_title_three1', 500)->comment('home_seven_first_title');
			$table->text('third_mid_desc_three1', 65535)->comment('home_seven_second_content');
			$table->string('mobile_app_bg_img1', 350)->comment('home_seven_banner_image');
			$table->string('vBannerBgImage', 500);
			$table->string('vBannerLeftImg', 500);
			$table->string('vBannerRightTitle');
			$table->string('vBannerRightTitleSmall');
			$table->text('tBannerRightContent', 65535);
			$table->string('vDeliveryPartTitle');
			$table->text('vDeliveryPartContent');
			$table->string('vDeliveryPartBgImg');
			$table->string('vDeliveryPartImg');
			$table->string('vMidSectionTitle');
			$table->string('vMidFirstImg', 500);
			$table->string('vMidFirstTitle');
			$table->text('tMidFirstContent', 65535);
			$table->string('vMidSecondImg', 500);
			$table->string('vMidSecondTitle');
			$table->text('tMidSecondContent', 65535);
			$table->string('vMidThirdImg', 500);
			$table->string('vMidThirdTitle');
			$table->text('tMidThirdContent', 65535);
			$table->string('vThirdSectionImg1', 500);
			$table->string('vThirdSectionImg2', 500);
			$table->string('vThirdSectionImg3', 500);
			$table->string('vThirdSectionRightTitle');
			$table->text('tThirdSectionRightContent', 65535);
			$table->string('vThirdSectionAPPImgAPPStore', 500);
			$table->string('vThirdSectionAPPImgPlayStore', 500);
			$table->string('vLastSectionTitle');
			$table->string('vLastSectionImg', 500);
			$table->string('vLastSectionFirstTitle');
			$table->text('tLastSectionFirstContent', 65535);
			$table->string('vLastSectionSecondTitle');
			$table->text('tLastSectionSecondContent', 65535);
			$table->string('vLastSectionThirdTitle');
			$table->text('tLastSectionThirdContent', 65535);
			$table->string('vLastSectionFourthTitle');
			$table->text('tLastSectionFourthContent', 65535);
			$table->string('vLastSectionFifthTitle');
			$table->text('tLastSectionFifthContent', 65535);
			$table->string('vLastSectionSixthTitle');
			$table->text('tLastSectionSixthContent', 65535);
			$table->string('BannerBgImage', 500);
			$table->string('BannerBigTitle');
			$table->string('BannerSmallTitle');
			$table->string('BannerContent', 500);
			$table->string('FirstSectionLeftImage', 500);
			$table->text('FirstSectionHeading', 65535);
			$table->text('FirstParaTitle', 65535);
			$table->text('FirstParaContent', 65535);
			$table->text('SecondParaTitle', 65535);
			$table->text('SecondParaContent', 65535);
			$table->text('ThirdParaTitle', 65535);
			$table->text('ThirdParaContent', 65535);
			$table->text('MidFirstImage', 65535);
			$table->text('MidFirstTitle', 65535);
			$table->text('MidFirstContent', 65535);
			$table->text('MidSecImage', 65535);
			$table->text('MidSecTitle', 65535);
			$table->text('MidSecContent', 65535);
			$table->text('MidThirdImage', 65535);
			$table->text('MidThirdTitle', 65535);
			$table->text('MidThirdContent', 65535);
			$table->text('ThirdLeftImg1', 65535);
			$table->text('ThirdLeftImg2', 65535);
			$table->text('ThirdLeftImg3', 65535);
			$table->text('ThirdRightTitle', 65535);
			$table->text('ThirdRightContent', 65535);
			$table->text('PlayStoreImg', 65535);
			$table->text('AppStoreImg', 65535);
			$table->text('AboutUsBgImage', 65535);
			$table->text('AboutUsTitle', 65535);
			$table->text('AboutUsSecondTitle', 65535);
			$table->text('AboutUsContent', 65535);
			$table->text('HomeRestuarantSectionLabel', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('homecontentfood');
	}

}
